package ejerciciosRecursividad;

public class RecursionArray {
	
//	 calcula la suma de todos los elementos del array
	public static int sumarElementos(int[] elementos, int i) {
		
		if(i == elementos.length) {
			return 0;
			
		} return elementos[i] + sumarElementos(elementos, i+1);
	}

	
//	Devuelve true si un numero esta en el array,  false en caso contrario
	public static boolean buscarElemento(int[] a, int item, int i) {
		
		//si recorre todo el arreglo y el elemento no esta entonces devuelvo false
		if(i == a.length) {
			return false;
		}
		
		if(a[i] == item) {
			return true;
			
		} return buscarElemento(a, item, i+1);
			
	}
	
//	Devuelve el máximo elemento de un array.
	public static int maximoRecursivo(int[] a, int i) {
		
		int max;
		
	    if (i == a.length-1) {
	        max = a[i];
	        
	    } else if(a[i] > maximoRecursivo(a, i+1)) {
		    	   max = a[i];
		    	   
	    } else {
	    	   max = maximoRecursivo(a,i+1); 
	    }
		return max;
	}
	
	
	
	public static void main(String[] args) {
		int [] array = {6,5,7};
		
	System.out.println(sumarElementos(array, 0));
//	System.out.println(buscarElemento(array,4,0));
//	System.out.println(maximoRecursivo(array,0));
	
	
	}

}
